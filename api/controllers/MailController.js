/**
 * MailController
 *
 * @description :: Server-side logic for managing mails
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var nodemailer = require('nodemailer');

var transport;


transport = nodemailer.createTransport("SMTP", {
    host: "192.168.0.208", // hostname
    auth: {
	    user: process.env.STMP_USER,
	    pass: process.env.STMP_PASS
    }
});

if(process.env.TEST_MODE){
	// create reusable transporter object using SMTP transport
	transporter = nodemailer.createTransport({
	    service: 'Gmail',
	    auth: {
	        user: process.env.GMAIL_USER || sails.config.GMAIL_USER,
	        pass: process.env.GMAIL_PW || sails.config.GMAIL_PW
	    }
	});
}

module.exports = {

	test: function(req, res){
		EmailService.sendWorkOrderEmail({});
		res.json({status: 'success'});
	},

  /**
   * `MailController.send()`
   */
  send: function (req, res) {
  	// setup e-mail data with unicode symbols
	var mailOptions = {
	    from: 'Yamaha Publications  <ld.studio.ca+ymus@gmail.com>', // sender address
	    to: 'larry_rivera@mac.com', // list of receivers
	    subject: 'Hello', // Subject line
	    text: 'Hello world ', // plaintext body
	    html: '<b>Hello world ✔</b>' // html body
	};

	// send mail with defined transport object
	transporter.sendMail(mailOptions, function(error, info){
	    if(error){
	        console.log(error);
	    }else{
	        console.log('Message sent: ' + info.response);
	    }
	});

    return res.json({
      todo: 'Email Sent'
    });
  }
};

