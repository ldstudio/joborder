/**
 * WorkorderController
 *
 * @description :: Server-side logic for managing workorders
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var filesize = require('file-size');

module.exports = {

	/* Gets work order json by id */
	get: function(req, res){

			Workorder.find({
				'wid':req.params.id
			}).exec(function(err,workorder){
					res.json(workorder);
			});
	},

	file: function(req, res){
		var blobAdapter = require('skipper-gridfs')({
    		uri: 'mongodb://localhost:27017/workorder.uploads'
    	});


		var fd = req.params.id; // value of fd comes here from get request

		File.findOne({'fd':fd}).exec(function(err,file){
			if(err) return res.json({error:"error"});
			
			blobAdapter.read(fd, function(error , gdfile) {
				if(error) {
					//res.json(error);
					res.notFound();
				} else {
					res.setHeader('Content-disposition', 'attachment; filename=' + file.filename);
					res.contentType(file.type);
					res.send(new Buffer(gdfile));
				}
			});

		});

	},


	/* Gets file for download */
	/**
		TODO: - get filename from files collection
	**/
	fileById: function(req, res){
		var blobAdapter = require('skipper-gridfs')({
    	uri: 'mongodb://localhost:27017/workorder.uploads'
    });

		var fd = req.params.id; // value of fd comes here from get request
			blobAdapter.read(fd, function(error , file) {

				if(error) {
						res.json(error);
					} else {
						res.contentType('application/octet-stream');
						res.send(new Buffer(file));
					}
				});
	},

	/**
	* Shortlink method
	**/
	s: function(req, res){
		console.log(req.params.id);
		Workorder.findOne({'shortid':req.params.id})
		.exec(function(err, workorder){
			if(err){
				console.log('shortid not found');
				res.notFound();

			}else{
				workorder.host = process.env.HOST_URL || 'localhost:8080'
				console.log(workorder);
				res.view('workorder',workorder);
			}
		});
	},

	add: function(req, res) {

		//console.log(req.params.all());
		//we first uload files to gridfs

		req.file('file')
		.upload({
			adapter: require('skipper-gridfs'),
			uri: 'mongodb://localhost:27017/workorder.uploads'
		}, function whenDone(err, uploadedFiles){
			if(err){ return res.negotiate(err);
			}else{
				console.log(uploadedFiles);
				var files = [];
				_.each(uploadedFiles, function(ufile){
					console.log(ufile);
					files.push({
						fd: ufile.fd,
						filename: ufile.filename,
						type: ufile.type,
						size: ufile.size,
						sizeHR: filesize(ufile.size).human({ si: true }),
					})

				});

				Workorder.create({
							jobName: req.param('jobName'),
							edate: req.param('edate'),
							ddate: req.param('ddate'),
							gl: req.param('gl'),
							dds: req.param('dds'),
							requester: req.param('requester'),
							phone: req.param('phone'),
							qty: req.param('qty'),
							email: req.param('email'),
							jobType: req.param('jobType'),
							deliver_to_requester: req.param('deliver_to_requester'),
							plan_attached: req.param('plan_attached'),
							mail: req.param('mail'),
							stock: req.param('stock'),
							otherDist: req.param('otherDist'),
							otherDistText: req.param('otherDistText'),
							allsupplied: req.param('allsupplied'),
							supplied_later : req.param('suppliedlater'),
							items: req.param('items'),
							proof: req.param('proof'),
							description: req.param('description'),
							files: files

				}).exec(function(err, workorder){
					if(!err) {
						//@delete the file
						//console.log(workorder);

						EmailService.sendWorkOrderEmail({
							name: workorder.requester,
							email: 'ld.studio.ca@gmail.com',
							wid: workorder.wid,
							order: workorder,
							host: process.env.HOST_URL || 'localhost:8080'
						});

						res.json({
							success: true,
							workorder: workorder
						});
					}else{
						console.log(err);
						res.json({
							error: true,
							error: err
						});
					}
				});

			}

		})

	}

};
