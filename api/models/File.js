/**
* File.js
*
* @description :: Save File metadata to seperate collection
*/

module.exports = {

  attributes: {
  	fd: 'string',
	filename: 'string',
	type: 'string',
	size: 'integer',
	sizeHR: 'string',
	wid: 'integer'
  }
};

