/**
* Workorder.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var shortId = require('shortid');
var Promise = require('promise');
var filesize = require('file-size');

module.exports = {

	attributes: {
		wid: 'integer',
		shortid: 'string',
		jobName: 'string',
		edate: 'date',
		ddate: 'date',
		gl: 'integer',
		dds: 'string',
		requester: 'string',
		phone: 'string',
		qty: 'integer',
		email: 'string',
		jobType: 'string',
		deliver_to_requester: 'boolean',
		plan_attached: 'boolean',
		mail: 'boolean',
		stock: 'boolean',
		otherDist: 'boolean',
		otherDistText: 'string',
		allsupplied: 'boolean',
		supplied_later : 'boolean',
		items: 'array',
		proof: 'json',
		description: 'text',
		files:'array'
	},

	beforeCreate : function(workorder, cb){
	    //Auto increment workaround
	    console.log("workorder created");
	    var incModel = "Workorder";

	    var sid = shortId.generate();

	    Counter.findOne({"model_name": incModel}).exec(function(err, counter){
	        if (err) return err;
	        if(counter){
	        	console.log("counter::::");
	        	console.log(counter.amount + 1);
	            var newAmount = counter.amount + 1;
	            counter.amount = newAmount;

	            counter.save(function(err, c){
	                //Error handling...
	                workorder.wid = newAmount;
	                workorder.shortid = sid;
	                cb();
	            });
	        }else{
	            cb();
	        }
	    });
	},

	afterCreate: function(workorder, cb){

		function saveImageData(workorder){

			return new Promise(function(fulfill, reject){
				if(workorder.files.length  < 1) return  fulfill();

				var count = 0

				_.each(workorder.files, function(file){

					File.create({
					  	fd: file.fd,
						filename: file.filename,
						type: file.type,
						size: file.size,
						sizeHR: filesize(file.size).human({ si: true }),
						wid: workorder.wid
					}).exec(function(err, file){
						if(err) console.log(err);
						console.log('FilesCount');
						console.log(count);
						count++;
						if(count === workorder.files.length)
							console.log('fulfill called');
							fulfill();

					})

				});

			})

		}

		saveImageData(workorder).then(cb);
	}
};
