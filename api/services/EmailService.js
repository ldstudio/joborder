// EmailService.js - in api/services


var nodemailer = require('nodemailer');

var transport;

var path = require('path');
var templatesDir = path.join(__dirname, '../..','templates');
var emailTemplates = require('email-templates')


transport = nodemailer.createTransport("SMTP", {
    host: "192.168.0.208", // hostname
    auth: {
      user: process.env.STMP_USER,
      pass: process.env.STMP_PASS
    }
});

if(process.env.GMAIL_USER){
  // create reusable transporter object using SMTP transport
  transporter = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
          user: sails.config.GMAIL_USER || process.env.GMAIL_USER,
          pass: sails.config.GMAIL_PW || process.env.GMAIL_PW
      }
  });
}

exports.sendWorkOrderEmail = function(options) {


    emailTemplates(templatesDir, function(err, template) {

      if (err) {
        console.log(err);
      } else {
        // An example users object with formatted email function
        var locals = {
          email: 'larry_rivera@mac.com',
          name: {
            first: 'Larry',
            last: 'Rivera'
          },
          order: options.order,
          wid: options.wid,
          host: options.host
        };


        // Send a single email
        template('workorder', locals, function(err, html, text) {
          console.log(locals);
          //console.log(text);

          if (err) {
            console.log(err);
          } else {

                  // // setup e-mail data with unicode symbols
            var mailOptions = {
                from: 'Yamaha Publications  <ld.studio.ca+ymus@gmail.com>', // sender address
                to: 'larry_rivera@mac.com', // list of receivers
                subject: 'Workorder - ' + locals.wid, // Subject line
                text: text, // plaintext body
                html: html // html body
            };
            //send mail with defined transport object
            transporter.sendMail(mailOptions, function(error, info){
                if(error){
                    console.log(error);
                }else{
                    console.log('Message sent: ' + info.response);
                }
            });

          }
        });





      }

  });

}
