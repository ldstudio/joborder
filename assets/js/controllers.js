angular.module('JobOrder.controllers', ['sails.io','ui.bootstrap', 'angularFileUpload']).
controller('ModalInstanceCtrl', function($scope,$modalInstance, files, order, wid,sid) {

    $scope.order = order;
    $scope.wid = wid;
    $scope.files = files;
    $scope.sid = sid;



    $scope.ok = function () {
      $modalInstance.dismiss('cancel');
      window.location = '/';
    };
})
.controller('MainCtrl', function($scope, $http, $sailsSocket, $filter, $upload, $modal) {

  console.log(1);

    $scope.formErrors= [];
    $scope.files= [];
    $scope.dynamic = 0;
    $scope.type = 'info';
    $scope.pb = false;
    $scope.selectedFile = 0;


  $scope.$watch('order.jobType', function(newVal, oldVal) {
    if(newVal !== ' Other'){
      if($scope.order.otherField)
        delete $scope.order.otherField;
    }
  })

  $scope.$watch('order.otherDist', function(newVal, oldVal) {
    if(newVal == false){
      if(typeof $scope.order.otherDist != 'undefined'){
        delete $scope.order.otherDist;
        delete $scope.order.otherDistText;
      }
    }
  });

  $scope.$watch('order.allsupplied', function(newVal, oldVal){
    if(newVal == true) $scope.order.suppliedlater = false;
  });


  $scope.$watch('order.suppliedlater', function(newVal, oldVal){
    if(newVal == true) $scope.order.allsupplied = false;
  });


  $scope.$watch('order.qty', function(newVal, oldVal){
    if(newVal <= -1) $scope.order.qty = 0;
  });

  $scope.clearForm = function() {
    $scope.order = {
      items:[]
    };
    $scope.files = [];
  };

  $scope.clearForm();

  $scope.showFile = function(file, index){
    console.log(file);
    $scope.selectedFile = index;
  };

  $scope.validate = function() {
    //Jobname
    if(!$scope.order.jobName){
      $('.jobName').addClass('has-error');
      $scope.formErrors.push("Job Name is a require field.");
    }else{
      $('.jobName').removeClass('has-error').addClass('has-success');
    }

    //Div Dept Sec
    if(!$scope.order.dds){
      $('.dds').addClass('has-error');
      $scope.formErrors.push("Div/Dept/Sec is a require field.");
    }else if(! $scope.order.dds.match(/^[0-9]{2}[\/][0-9]{2}[\/][0-9][0-9]$/) ){
      $('.dds').addClass('has-error');
      $scope.formErrors.push("Div/Dept/Sec format is 00/00/00");
    }else{
      $('.dds').removeClass('has-error').addClass('has-success');
    }

    //Requester
    if(!$scope.order.requester){
      $('.requester').addClass('has-error');
      $scope.formErrors.push("Requester is a require field.");
    }else{
      $('.requester').removeClass('has-error').addClass('has-success');
    }

    //GL
    if(!$scope.order.gl){
      $('.gl').addClass('has-error');
      $scope.formErrors.push("GL is a require field.");
    }else if($scope.order.gl < 10001 || $scope.order.gl > 99999 ){
      $('.gl').addClass('has-error');
      $scope.formErrors.push("GL is out of range.");
    }else{
      $('.gl').removeClass('has-error').addClass('has-success');
    }

    //Email
    if(!$scope.order.email){
      $('.email').addClass('has-error');
      $scope.formErrors.push("e-mail is a require field.");
    }else{
      $('.email').removeClass('has-error').addClass('has-success');
    }


    if ($scope.formErrors.length > 0){
      console.log($scope.formErrors.join(', '));
      $scope.formErrors = [];
      return false;
    }else{
      //submit the form
      return true;
    }

    $scope.formErrors = [];
  }


  $scope.submitForm = function(){
    console.dir($scope.order);
    if($scope.validate()){

      ga('send', {
        'hitType': 'event',          // Required.
        'eventCategory': 'button',   // Required.
        'eventAction': 'submit',      // Required.
        'eventLabel': 'joborder'
      });

      $scope.upload($scope.files);

      //add form clear and popup with permlink
    }
  };

  $scope.openModal = function (wid,files,sid) {
    var modalInstance = $modal.open({
      templateUrl: 'submit.html',
      controller: 'ModalInstanceCtrl',
      size: 'sm',
      resolve: {
        files: function(){
          return files;
        },
        order: function(){
          return $scope.order;
        },
        wid: function(){
          return wid;
        },
        sid: function(){
          return sid;
        }
      }
    });


  };

  $scope.upload = function (files) {

      $upload.upload({
          url: '/workorder/add',
          fields: $scope.order,
          file: files
      }).progress(function (evt) {
          $scope.pb = true;
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
          $scope.dynamic = progressPercentage;
      }).success(function (data, status, headers, config) {

          $scope.type = 'success';
          console.log(data);
          debugger;
          $scope.openModal(data.workorder.wid, data.workorder.files, data.workorder.shortid );
          //console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
      }).error(function(){
          $scope.dynamic = 10
          $scope.type = 'warning';

      });

  };


  $(function () {
      $('#edate').datetimepicker({
        format: 'MM/DD/YYYY'
      });
      $('#ddate').datetimepicker({
        format: 'MM/DD/YYYY'
      });


      $('#DatePromised1').datetimepicker({
        format: 'MM/DD/YYYY'
      });

      $('#DatePromised2').datetimepicker({
        format: 'MM/DD/YYYY'
      });

      $('#DatePromised3').datetimepicker({
        format: 'MM/DD/YYYY'
      });

      $("#edate").on("dp.change",function (e) {
          $('#ddate').data("DateTimePicker").minDate(e.date);
          $scope.order.edate = e.date.format('MM/DD/YYYY');

      });
      $("#ddate").on("dp.change",function (e) {
          $('#edate').data("DateTimePicker").maxDate(e.date);
          $scope.order.ddate = e.date.format('MM/DD/YYYY');
      });

  });

});
