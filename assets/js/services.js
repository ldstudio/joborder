angular.module('JobOrder.services', []).
  factory('modelAPIService', function($http) {

    var modelAPI = {};

    modelAPI.getDrivers = function() {
      return $http({
        method: 'JSONP', 
        url: 'http://ergast.com/api/f1/2013/driverStandings.json?callback=JSON_CALLBACK'
      });
    }

    return modelAPI;
  });